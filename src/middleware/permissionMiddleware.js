const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

// Errors
const {ForbiddenError} = require('../util/customErrors');

const checkPermission = async (req, expectedRole, next) => {
  const {userId} = req.user;
  const {role} = await User.findOne({_id: userId});
  if (role !== expectedRole) {
    next(new ForbiddenError(
        `Only ${expectedRole}s have permission to perform this action!`,
    ));
  }
  next();
};

const checkDriverPermission = async (req, res, next) => {
  await checkPermission(req, 'DRIVER', next);
};

const checkShipperPermission = async (req, res, next) => {
  await checkPermission(req, 'SHIPPER', next);
};

const checkAssignmentStatus = async (req, res, next) => {
  const {userId} = req.user;
  const {role} = await User.findOne({_id: userId});
  const onloadAmount = await Truck
      .countDocuments({assigned_to: userId, status: 'OL'});

  if (role === 'DRIVER' && onloadAmount > 0) {
    next(new ForbiddenError('Forbiden, you are on load!'));
  }

  next();
};

module.exports = {
  checkDriverPermission,
  checkShipperPermission,
  checkAssignmentStatus,
};
