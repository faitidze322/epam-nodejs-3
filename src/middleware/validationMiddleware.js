const Joi = require('joi');

// Errors
const {InvalidRequestError} = require('../util/customErrors');

// Predefined validation fields
const email = Joi.string().email().required();
const password = Joi.string().min(6).max(20).required();

// Error handling
const handleError = async (schema, req, next) => {
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(new InvalidRequestError(err.message));
  }
};

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    email,
    password,
    role: Joi.string()
        .valid('SHIPPER', 'DRIVER')
        .required(),
  });

  await handleError(schema, req, next);
};

const loginValidator = async (req, res, next) => {
  const schema = Joi.object({
    email,
    password,
  });

  await handleError(schema, req, next);
};

const changePasswordValidator = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: password,
    newPassword: password,
  });

  await handleError(schema, req, next);
};

const truckValidation = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
  });

  await handleError(schema, req, next);
};

const loadValidation = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object({
      width: Joi.number().required(),
      length: Joi.number().required(),
      height: Joi.number().required(),
    }).required(),
  });

  await handleError(schema, req, next);
};

module.exports = {
  registrationValidator,
  loginValidator,
  changePasswordValidator,
  truckValidation,
  loadValidation,
};
