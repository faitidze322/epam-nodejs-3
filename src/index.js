require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();

const PORT = process.env.PORT || 8080;

// Errors
const {NodeCourseError} = require('./util/customErrors');

// Controllers
const {authRouter} = require('./controllers/authController');
const {userDataRouter} = require('./controllers/userDataController');
const {trucksRouter} = require('./controllers/trucksController');
const {loadsRouter} = require('./controllers/loadsController');

// Middleware
const {authMiddleware} = require('./middleware/authMiddleware');
const {
  checkDriverPermission,
} = require('./middleware/permissionMiddleware');

app.use(express.static('./dist'));
app.use(express.json());
app.use(morgan('tiny'));

// Routers
app.use('/api/auth', authRouter);
app.use('/api/users/me', [authMiddleware], userDataRouter);
app.use('/api/trucks', [authMiddleware, checkDriverPermission], trucksRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

// Start
(async () => {
  try {
    await mongoose.connect(
        process.env.DB,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          useCreateIndex: true,
        },
    );
    app.listen(PORT, () => {
      console.log(`Server started on port ${PORT}`);
    });
  } catch (err) {
    console.error(`Startup server error: ${err}`);
  }
})();
