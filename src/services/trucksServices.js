const {Truck} = require('../models/truckModel');

// Errors
const {
  InvalidRequestError,
} = require('../util/customErrors');

const getUserTrucksById = async (userId) => {
  try {
    return await Truck
        .find({created_by: userId})
        .select(['-__v']);
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const createTruck = async (type, userId) => {
  const truck = new Truck({
    type,
    created_by: userId,
  });

  await truck.save();
};

const getUsersTruckByItsId = async (userId, truckId) => {
  try {
    return await Truck
        .findOne({created_by: userId, _id: truckId})
        .select(['-__v']);
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const updateUsersTruckById = async (userId, truckId, newData) => {
  try {
    const {assigned_to} = await Truck.findById(truckId);
    if (assigned_to == userId) {
      throw new Error('You can`t edit this truck now, it is assigned to you.');
    }

    await Truck
        .findOneAndUpdate(
            {created_by: userId, _id: truckId},
            {$set: newData},
        );
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const deleteUserTruckById = async (userId, truckId) => {
  try {
    const {assigned_to} = await Truck.findById(truckId);
    if (assigned_to == userId) {
      throw new Error('You can`t edit this truck now, it is assigned to you.');
    }

    await Truck
        .findOneAndRemove(
            {created_by: userId, _id: truckId},
        );
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const assignTruckToUserById = async (userId, truckId) => {
  try {
    await Truck
        .findByIdAndUpdate(
            truckId,
            {assigned_to: userId},
        );
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

module.exports = {
  getUserTrucksById,
  createTruck,
  getUsersTruckByItsId,
  updateUsersTruckById,
  deleteUserTruckById,
  assignTruckToUserById,
};
