const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User} = require('../models/userModel');

// Errors
const {
  InvalidRequestError,
} = require('../util/customErrors');

const registration = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  if (role == 'DRIVER') {
    user.status = 'IS';
  }

  await user.save();
};

const signIn = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user || !await bcrypt.compare(password, user.password)) {
    throw new InvalidRequestError('Incorrect password or email');
  }

  const jwt_token = jwt.sign({
    _id: user._id,
    email: user.email,
  }, 'secret');

  return jwt_token;
};

module.exports = {
  registration,
  signIn,
};
