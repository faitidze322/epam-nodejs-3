const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

// Errors
const {
  InvalidRequestError,
} = require('../util/customErrors');

const getUserInfoById = async (userId) => {
  return await User
      .findOne({_id: userId})
      .select(['-password', '-__v']);
};

const deleteUserById = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

const changeUserPasswordById = async (
    userId,
    passwordFromInput,
    newPasswordFromInput,
) => {
  const {password: passwordInDB} = await User.findOne({_id: userId});
  const isCorrectPassword = await bcrypt
      .compare(passwordFromInput, passwordInDB);

  if (!isCorrectPassword) {
    throw new InvalidRequestError('Incorrect password');
  }

  const newPassword = await bcrypt.hash(newPasswordFromInput, 10);
  await User
      .findOneAndUpdate(
          {_id: userId},
          {password: newPassword},
      );
};

module.exports = {
  getUserInfoById,
  deleteUserById,
  changeUserPasswordById,
};
