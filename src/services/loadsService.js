// Model
const {User} = require('../models/userModel');
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');

// Error
const {
  InvalidRequestError,
} = require('../util/customErrors');

const iterateState = (current) => {
  const stateLevels = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ];
  const currentLvlIndex = stateLevels.findIndex(current);

  return stateLevels[currentLvlIndex + 1];
};

const createUserLoad = async (data, userId) => {
  const load = new Load(data);

  load.created_by = userId;
  await load.save();
};

const getUserLoads = async (userId) => {
  try {
    const {role} = await User.findById(userId);

    switch (role) {
      case 'DRIVER': {
        return await Load.find({assigned_to: userId}).select(['-__v']);
      };
      case 'SHIPPER': {
        return await Load.find({}).select(['-__v']);
      };
      default:
        break;
    }
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const getUserActiveLoad = async (userId) => {
  const load = await Load
      .findOne({
        assigned_to: userId,
        status: 'ASSIGNED',
      }).select(['-__v']);

  return load;
};

const postUserLoadById = async (userId, loadId) => {
  const truck = await Truck.findOne({
    status: 'IS',
    assigned_to: {$ne: null},
  });

  if (!truck) {
    return false;
  }

  const load = await Load.findById(loadId);

  truck.status = 'OL';
  load.status = 'ASSIGNED';
  load.state = 'En route to Pick Up';
  load.created_by = userId;
  load.assigned_to = truck.assigned_to;

  await truck.save();
  await load.save();

  return true;
};

module.exports = {
  createUserLoad,
  getUserLoads,
  getUserActiveLoad,
  postUserLoadById,
};
