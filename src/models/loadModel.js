const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  logs: {
    type: Array,
    default: [],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Load};
