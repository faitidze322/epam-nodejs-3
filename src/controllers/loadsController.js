const express = require('express');
const router = express.Router();

// Services
const {
  createUserLoad,
  getUserLoads,
  getUserActiveLoad,
  postUserLoadById,
} = require('../services/loadsService');

// Util
const {
  asyncWrapper,
} = require('../util/apiUtils');

// Validation
const {
  loadValidation,
} = require('../middleware/validationMiddleware');

// Permission
const {
  checkDriverPermission,
  checkShipperPermission,
} = require('../middleware/permissionMiddleware');

router.post(
    '/',
    checkShipperPermission,
    loadValidation,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;

      await createUserLoad(req.body, userId);

      res.status(200).json({message: 'Load created successfully'});
    }));

router.get(
    '/',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const loads = await getUserLoads(userId);

      res.status(200).json({loads});
    }));

router.get(
    '/active',
    checkDriverPermission,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const load = await getUserActiveLoad(userId);

      res.status(200).json({load});
    }));

router.post(
    '/:loadId/post',
    checkShipperPermission,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {loadId} = req.params;

      const isSuccess = await postUserLoadById(userId, loadId);

      if (isSuccess) {
        res.status(200)
            .json({message: 'Load posted successfully',
              driver_found: isSuccess});
      } else {
        res.status(200)
            .json({message: 'Driver not found',
              driver_found: isSuccess});
      }
    }));

module.exports = {
  loadsRouter: router,
};
