const express = require('express');
const router = express.Router();

// Services
const {
  getUserTrucksById,
  createTruck,
  getUsersTruckByItsId,
  updateUsersTruckById,
  deleteUserTruckById,
  assignTruckToUserById,
} = require('../services/trucksServices');

// Util
const {
  asyncWrapper,
} = require('../util/apiUtils');

// Validation
const {
  truckValidation,
} = require('../middleware/validationMiddleware');

// Permission
const {
  checkAssignmentStatus,
} = require('../middleware/permissionMiddleware');

router.get(
    '/',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const trucks = await getUserTrucksById(userId);

      res.status(200).json({trucks});
    }));

router.post(
    '/',
    checkAssignmentStatus,
    truckValidation,
    asyncWrapper(async (req, res) => {
      const {type} = req.body;
      const {userId} = req.user;

      await createTruck(type, userId);
      res.status(200).json({message: 'Truck created successfully'});
    }));

router.get(
    '/:truckId',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {truckId} = req.params;

      const truck = await getUsersTruckByItsId(userId, truckId);
      res.status(200).json({truck});
    }));

router.put(
    '/:truckId',
    checkAssignmentStatus,
    truckValidation,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {truckId} = req.params;

      await updateUsersTruckById(userId, truckId, req.body);
      res.status(200).json({message: 'Truck details changed successfully'});
    }));

router.delete(
    '/:truckId',
    checkAssignmentStatus,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {truckId} = req.params;

      await deleteUserTruckById(userId, truckId);
      res.status(200).json({message: 'Truck deleted successfully'});
    }));

router.post(
    '/:truckId/assign',
    checkAssignmentStatus,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {truckId} = req.params;

      await assignTruckToUserById(userId, truckId);
      res.status(200).json({message: 'Truck assigned successfully'});
    }));

module.exports = {
  trucksRouter: router,
};
