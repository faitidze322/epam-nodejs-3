const express = require('express');
const router = express.Router();

// Services
const {
  getUserInfoById,
  deleteUserById,
  changeUserPasswordById,
} = require('../services/userDataService');

// Util
const {
  asyncWrapper,
} = require('../util/apiUtils');

// Validation
const {
  changePasswordValidator,
} = require('../middleware/validationMiddleware');

// Permission
const {
  checkAssignmentStatus,
} = require('../middleware/permissionMiddleware');

router.get('/', asyncWrapper(async (req, res) => {
  const {
    userId,
  } = req.user;

  const data = await getUserInfoById(userId);
  res.status(200).json(data);
}));

router.delete(
    '/',
    checkAssignmentStatus,
    asyncWrapper(async (req, res) => {
      const {
        userId,
      } = req.user;

      await deleteUserById(userId);
      res.status(200).json({message: 'Profile deleted successfully'});
    }));

router.patch(
    '/password',
    checkAssignmentStatus,
    changePasswordValidator,
    asyncWrapper(async (req, res) => {
      const {
        userId,
      } = req.user;
      const {
        oldPassword,
        newPassword,
      } = req.body;

      await changeUserPasswordById(
          userId, oldPassword, newPassword,
      );
      res.status(200).json({message: 'Password changed successfully'});
    }));

module.exports = {
  userDataRouter: router,
};
